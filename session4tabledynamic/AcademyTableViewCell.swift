//
//  AcademyTableViewCell.swift
//  session4tabledynamic
//
//  Created by Citra Indra Pradita on 02/12/22.
//

import UIKit

class AcademyTableViewCell: UITableViewCell {

    @IBOutlet weak var acadenyLabel: UILabel!
    @IBOutlet weak var academyImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
